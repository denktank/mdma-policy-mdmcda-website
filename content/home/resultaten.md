---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 15

title: Resultaten
subtitle:

design:
  columns: "1"
  background:
    color: #ffffff
    text_color_light: false
  spacing:
    padding: ["20px", "0", "20px", "0"]
---

![De resultaten van de Denktank MDMA-beleid: te verwachten verbeteringen en verslechteringen van vijf beleidsmodellen, met de hoogst en laagst haalbare scores als kader.](images/main-results-figure.png)

Het Nederlandstalige rapport met de bevindingen van de denktank is [hier beschikbaar](https://mdmabeleid.nl/rapport). De volledige analyses zijn [hier beschikbaar](https://mdmabeleid.nl/results). De preprint van het Engelstalige artikel is  [hier beschikbaar](https://doi.org/10.31219/osf.io/txy5z). 

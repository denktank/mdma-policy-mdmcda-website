---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

title: Samenvatting
subtitle:

design:
  columns: "1"
  background:
    color: #ffffff
    text_color_light: false
  spacing:
    padding: ["20px", "0", "20px", "0"]
---

Ondanks de relatief lage verslavings- en gezondheidsrisico’s is MDMA (ecstasy), net als amfetamine, cocaïne en heroïne, geclassificeerd als een ‘harddrug’ (Lijst I van de Opiumwet). In Nederland is veel zware criminaliteit betrokken bij de illegale productie, verkoop en export van ecstasy. 

Een DenkTank van 18 experts heeft, op basis van de beschikbare wetenschappelijke kennis, onderzocht welke beleidsopties een bijdrage kunnen leveren aan het verminderen van de gezondheidsrisico’s en het bestrijden van de aan MDMA-gerelateerde criminaliteit. 

De experts hebben het effect van 95 mogelijke beleidsopties gescoord op 27 verschillende uitkomsten, zoals gezondheidsschade, criminaliteit, mate van gebruik, milieuschade en internationale imagoschade. Voorbeelden van beleidsopties zijn het al dan niet toestaan van reclame, vergunning voor de verkoop, een hardere aanpak van de misdaad, kwaliteitsbewaking van producten, drugsvoorlichting en strafverzwaring. Omdat het belang (de ‘impact’) van de uitkomsten onderling nogal verschilt, zijn door het panel ook weegfactoren toegekend aan de 27 uitkomsten. 

De verwachte effecten van alle mogelijke beleidsopties werden door de experts op basis van wetenschappelijke kennis en ervaring, en los van een overkoepelende ideologie, gescoord op alle mogelijke uitkomsten. Met deze aanpak werd het optimale model voor het Nederlandse ecstasybeleid gegenereerd. 

Het optimale model is vooral gericht op de regulering van de productie en de aan- en verkoop van MDMA, kwaliteitsbewaking, een hardere aanpak van de misdaad en monitoring van gebruik. Toepassing van het optimale model zal leiden tot een afname van de gezondheidsschade, minder consumptie per gebruiker, minder criminaliteit en minder milieuschade. Wel zal toepassing van het optimale model mogelijk leiden tot een beperkte toename van het aantal MDMA-gebruikers. 

Met het oog op de (politieke) haalbaarheid hebben de experts het optimale model licht aangepast tot het zogenaamde X-shop model, dat vrijwel dezelfde gunstige uitkomsten heeft.

Het thans gepresenteerde X-shop model is bedoeld als eerste aanzet voor nieuw ecstasybeleid en kan na politieke en maatschappelijke discussies verder worden verfijnd. Daarna kan een implementatietraject worden gestart om van de huidige ongunstige situatie te komen tot een situatie waarmee zowel de gebruiker als de maatschappij optimaal gediend zijn. 



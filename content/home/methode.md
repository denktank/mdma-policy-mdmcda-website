---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Methode
subtitle:

design:
  columns: "1"
  background:
    color: #ffffff
    text_color_light: false
  spacing:
    padding: ["20px", "0", "20px", "0"]
---

De afgelopen jaren neemt de discussie over het Nederlandse drugsbeleid toe, terwijl tegelijkertijd zulke discussies vaak snel polariseren. Dit maakt het moeilijk om op een neutrale manier de mogelijke positieve en negatieve effecten van de beschikbare beleidsopties te beschouwen en dreigt evaluatie van en reflectie op het Nederlandse drugsbeleid te blokkeren.

In 2019 besloten Ton Nabben, Floor van Bakkum, Jan van Amsterdam, en Gjalt-Jorn Peters een denktank te starten om met een multidisciplinaire groep experts op een zo objectief mogelijke manier de effecten van verschillende beleidsopties in kaart te brengen.

Zij baseerden dit proces op de Multi Criterion Decision Analysis (MCDA) methode die eerder is toegepast om alcohol- en cannabisbeleid te evalueren ([Rogeberg et al., 2018](https://doi.org/10.1016/j.drugpo.2018.01.019)). Om de objectiviteit te verhogen is deze methode uitgebreid naar Multi Decision Multi Criteria Decision Analysis (MDMCDA).

In dit proces hebben de 18 experts in de denktank 22 beleidsinstrumenten gedefinieerd die elk uit 2-7 wederzijds uitsluitende beleidsopties bestonden (bijvoorbeeld: verkoop aan consumenten is illegaal of legaal; en het bezit van een gebruikershoeveelheid is illegaal, wordt gedoogd, of is legaal). Samen beschrijven de in totaal 95 beleidsopties de belangrijkste vrijheidsgraden in het Nederlandse drugsbeleid met betrekking tot MDMA. Vervolgens hebben de experts de 27 belangrijkste uitkomsten van drugsbeleid gedefinieerd (bijvoorbeeld: gezondheidsschade of georganiseerde misdaad). Hierna hebben ze ingeschat wat het effect van elk van de 95 beleidsopties op die 27 uitkomsten zou zijn (dit waren 2565 schattingen).

Vervolgens hebben de experts een aantal beleidsmodellen gedefinieerd, waarbij elk beleidsmodel bestaat uit één gekozen beleidsoptie per beleidsinstrument (bijvoorbeeld: verkoop aan consumenten is illegaal en het bezit van een gebruikershoeveelheid wordt gedoogd).

Tot slot hebben de experts de uitkomsten gewogen (bijvoorbeeld: gezondheid is het belangrijkst, daarna misdaad, en daarna economie).

Hier eindigde de bijdrage van de experts. Op dit punt is hun gezamenlijke expertise is vastgelegd in de schattingen, zijn de voorgedefinieerde beleidsmodellen gespecificeerd, en heeft de denktank wegingen van de uitkomsten gespecificeerd. Hierna rest alleen nog het doorrekenen om te kijken hoe elk model scoort. Dit is door het {mdmcda} R package gedaan: dit woog de schattingen en berekende de totaalscore voor elke beleidsoptie voor alle uitkomsten. Vervolgens zijn de totaalscores voor de gekozen beleidsoptie per beleidsmodel opgeteld om tot de totaalscore voor elk beleidsmodel te komen. Deze resultaten zijn gerapporteerd door de denktank stuurgroepleden in het Nederlandstalige rapport.

De experts hebben zich gebaseerd op een narrative review van de wetenschappelijke state of the art met betrekking tot MDMA. Deze review is [hier beschikbaar](https://mdmabeleid.nl/narrative-review-for-think-tank), en een geupdate versie is [hier beschikbaar](https://mdmabeleid.nl/narrative-review-updated).

Meer details over de procedure, en de resultaten, zijn beschikbaar in het [Nederlandstalige rapport met de bevindingen van de denktank](https://mdmabeleid.nl/rapport), de [volledige analyses](https://mdmabeleid.nl/results), en de [preprint van het Engelstalige artikel](https://doi.org/10.31219/osf.io/txy5z). 

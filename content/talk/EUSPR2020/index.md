---
abstract: |-
  Prevention often aims to minimize risk behaviour or promote healthy behaviour. Prevention campaigns, therefore, are often ultimately behaviour change interventions. To develop an effective behaviour change intervention, it is first necessary to understand the target behaviour. To this end, prevention practitioners and researchers first conduct determinant studies, often combining literature reviews with qualitative and quantitative empirical research. Such determinant studies yield a list of relevant determinants of behaviour. These are then targeted in a prevention campaign using matching behaviour change principles. However, the constructs studied in determinant studies are often ill-defined, causing homonymous constructs to differ per theory in terms of what they are (i.e., definition) and/or how they should be operationalized. This results in large heterogeneity in measurement, hinders the application of theory in the development of interventions, and thwarts the systematic testing and synthesis of behaviour change theory.
  
  We present PsyCoRe: an Open Access repository with psychological constructs, specifically a set of determinants drawn from various prevention science theories. These determinants have comprehensive definitions, as well as up to four distinct instructions. The first are instructions for developing measurement instruments, for determinant studies using a survey. The second are instructions for coding qualitative data, for determinant studies using interviews. The third are instructions for eliciting construct content, supporting the development of interview schemes for qualitative determinant studies. The fourth are instructions for coding measurement instruments for conducting literature reviews.
  
  We will briefly illustrate how this repository and the specifications contained therein can be used to support and optimize the development of theory- and evidence-based behaviour change interventions. Specifically, we will show how one would leverage this resource to conduct a systematic review, a qualitative study, and a quantitative study to obtain an overview of the most relevant determinants to target in a prevention campaign.
all_day: false
authors: ["Gjalt-Jorn Ygram Peters", "Tjeerd Idger de Zeeuw", "Anneloes Baan", "Rik Crutzen", "Catherine Bolman"]
date: "2020-10-08T00:00:00Z"
date_end: "2020-10-10T00:00:00Z"
event: EUSPR2020
event_url: https://euspr.org
featured: false
links:
- icon: twitter
  icon_pack: fab
  name: Follow
  url: https://twitter.com/matherion
location: Online
math: true
publishDate: "2020-10-01T00:00:00Z"
summary: An e-poster presented at EUSPR2020.
tags: []
title: EUSPR2020 E-poster
url_code: ""
url_pdf: "https://psycore.one/resources/presentations/peters--de-zeeuw--baan--crutzen--bolman---psycore---euspr2020.pdf"
url_slides: "https://psycore.one/resources/presentations/peters--de-zeeuw--baan--crutzen--bolman---psycore---euspr2020.png"
url_video: ""
---

The PsyCoRe.one repository if available [here](https://psycore.one).
